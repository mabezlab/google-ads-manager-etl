import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
import pg8000
import pyspark.sql.functions as f
from awsglue.context import DynamicFrame

args = getResolvedOptions(sys.argv, ['JOB_NAME', 'db_connection', 'db_name', 'db_catalog'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)

db_options = dict(glueContext.extract_jdbc_conf(args['db_connection']))
db_url = db_options["url"]
db_options["url"] = db_url + '/' + args['db_name']
db_host = db_url.replace('jdbc:postgresql://','')
db_port = db_host[db_host.find(':'):].replace(':','')
db_host = db_host.replace(':' + db_port, '')

# load setups
db_options["dbtable"] = "setup_campaigns"
setup_campaigns = glueContext.create_dynamic_frame_from_options('postgresql', connection_options = db_options)

setup_campaigns_all_df = setup_campaigns.toDF()
setup_campaigns_all_df = setup_campaigns_all_df.selectExpr('id as setup_campaign_id','client_customer_id','name as setup_name','uf as setup_uf', 'host','city_brand_model','display_exhibition','google_partners','group_max_ads_num','all_languages','recommended_seg','searching_region_seg','recommended_seg_exc','throw_strategy_id','budget','cpc_max_default','accelerated_exhibition','keywords_am','keywords_ex','keywords_fr','begin_date','end_date','actived','feirao as setup_feirao','generic_campaigns','generic_ads','activate_campaign_min_cars','created_at','updated_at','sync_state')

setup_campaigns_pre_df = setup_campaigns_all_df.filter(setup_campaigns_all_df.sync_state.isin(['transformed','transforming','loading','loaded','waiting', 'complete']))
setup_campaigns_df = setup_campaigns_pre_df.filter('feirao = True')
setup_campaigns_df_ids = [v.setup_campaign_id for v in setup_campaigns_df.select('setup_campaign_id').collect()]

# load sync_states_histories
db_options["dbtable"] = "sync_states_histories"
sync_states_histories = glueContext.create_dynamic_frame_from_options('postgresql', connection_options = db_options)
sync_states_histories_df = sync_states_histories.toDF()

# set state to waiting
sync_states_histories_waiting = sync_states_histories_df.filter("current = 't' AND state IN ('transformed','loading','loaded','waiting', 'complete') AND setup_id IN ('%s')" % "','".join(str(v) for v in setup_campaigns_df_ids))
sync_states_histories_waiting_ids = [v.id for v in sync_states_histories_waiting.select('id').collect()]
scope_setup_ids = [v.setup_id for v in sync_states_histories_waiting.select('setup_id').collect()]
scope_setup_idsf = "','".join(str(v) for v in scope_setup_ids)

conn = pg8000.connect(host=db_host, port=int(db_port), database=args['db_name'], user=db_options["user"], password=db_options["password"])
cur = conn.cursor()
cur.execute("update setup_campaigns set sync_state='transforming' where id IN (%s)" % ('0' if not setup_campaigns_df_ids else ",".join(str(v) for v in setup_campaigns_df_ids)))
cur.execute("update sync_states_histories set current='f', finished_at = NOW() where current='t' AND setup_id IN (%s)" % ('0' if not scope_setup_ids else ",".join(str(v) for v in scope_setup_ids)))
for setup_id in setup_campaigns_df_ids:
    cur.execute("insert into sync_states_histories (state, setup_id, created_at, updated_at) values ('transforming', %d, NOW(), NOW())" % setup_id)
cur.execute("update day_histories set current='f', finished_at = NOW() where current='t'")
cur.execute("insert into day_histories (state, created_at, updated_at) values ('transforming', NOW(), NOW())")
conn.commit()
cur.close()
conn.close()

# load target
target_csv = glueContext.create_dynamic_frame.from_catalog(database = args['db_catalog'], table_name = "target_csv", transformation_ctx = "target_csv")
target_csv_df = target_csv.toDF().selectExpr(['`recsentity.id` as id', '`entity.name` as name', '`entity.categoryid` as categoryid', '`entity.message` as message', '`entity.thumbnailurl` as thumbnail_url', '`entity.value` as value', '`entity.pageurl` as page_url', '`entity.inventory` as inventory', '`entity.margin` as margin', '`entity.brand` as brand', '`entity.modelo` as modelo', '`entity.versao` as versao', '`entity.km` as km', '`entity.anomodelo` as ano_modelo', '`entity.anofabricacao` as ano_fabricacao', '`entity.blindado` as blindado', '`entity.uf` as uf', '`entity.cidade` as cidade', '`entity.cor` as cor', '`entity.nomecor` as nome_cor', '`entity.categoria` as categoria', '`entity.vendedor` as vendedor', '`entity.make` as make', '`entity.model` as model', '`entity.version` as version', '`entity.valor` as valor', '`entity.catpreco` as catpreco', '`entity.badge` as feirao', '`entity.feiroes` as feiroes', '`entity.listingtype` as info_neworold', '`entity.cnpj` as cnpj', '`entity.nomefantasia` as razao_social', '`entity.binaryone` as binary_one', '`entity.adjustpercentageone` as adjust_percentage_one', '`entity.randomone` as random_one', '`entity.binarytwo` as binary_two', '`entity.adjustpercentagetwo` as adjust_percentage_two', '`entity.randomtwo` as random_two', '`entity.lat` as latitude', '`entity.lon` as longitude'])

# load target setup ids
join_setup = target_csv_df.filter("feirao = '1'").join(setup_campaigns_df, f.concat_ws(' - ', f.trim(target_csv_df.feiroes), target_csv_df.uf) == f.concat_ws(' - ', f.trim(setup_campaigns_df.setup_name), setup_campaigns_df.setup_uf))
setups = join_setup.selectExpr('id','name','categoryid','message','thumbnail_url','value','page_url','CAST(inventory as INTEGER) as inventory','margin','brand','modelo','versao','km','ano_modelo','ano_fabricacao','blindado','uf','cidade','cor','nome_cor','categoria','vendedor','make','model','version','valor','catpreco','feirao','feiroes','cnpj','razao_social','info_neworold','binary_one','adjust_percentage_one','random_one','binary_two','adjust_percentage_two','random_two','latitude','longitude','setup_campaign_id')
vehicles = setups \
    .selectExpr("make as marca", "model as modelo", 'cidade', 'uf', 'setup_campaign_id', 'ano_fabricacao', 'nome_cor', 'value', 'inventory', "(CASE WHEN info_neworold = 'Novo' THEN true ELSE false END) as zerokm") \
    .groupBy('marca', 'modelo', 'cidade', 'uf', 'setup_campaign_id', 'zerokm') \
    .agg(f.concat_ws(";", f.collect_set(setups.ano_fabricacao)).alias('anos'), \
    f.concat_ws(";", f.collect_set(setups.nome_cor)).alias('cores'), \
    f.concat_ws(";", f.collect_set(setups.value)).alias('valores'), \
    f.sum('inventory').alias('quantidade'))

cities = vehicles.groupBy('cidade', 'uf', 'setup_campaign_id').count().select('cidade', 'uf', 'setup_campaign_id')

# persist worker_city
worker_city = cities.selectExpr("cidade as nome", "uf", 'setup_campaign_id')
db_options["dbtable"] = "worker_city"
worker_city_current = glueContext.create_dynamic_frame_from_options('postgresql', connection_options = db_options)
worker_city_current_df = worker_city_current.toDF()
worker_city_new = worker_city.alias("new") \
  .join(worker_city_current_df.alias("existing"), ((worker_city_current_df.nome == worker_city.nome) & (worker_city_current_df.uf == worker_city.uf) & (worker_city_current_df.setup_campaign_id == worker_city.setup_campaign_id)), "left_outer") \
  .where(worker_city_current_df.id.isNull()) \
  .select("new.*")

worker_city_dy = DynamicFrame.fromDF(worker_city_new, glueContext, "worker_city_dy")
worker_city_sy = glueContext.write_dynamic_frame.from_jdbc_conf(frame = worker_city_dy, catalog_connection = args['db_connection'], connection_options = {"dbtable": "worker_city", "database": args['db_name']}, transformation_ctx = "worker_city_sy")

worker_city = glueContext.create_dynamic_frame_from_options('postgresql', connection_options = db_options).toDF()
worker_city = worker_city.filter('setup_campaign_id IN (%s)'  % ('0' if not setup_campaigns_df_ids else ",".join(str(v) for v in setup_campaigns_df_ids)))

# persist vehicle_city
worker_cityb = worker_city.selectExpr("id as city_id", "nome as city_nome", "uf as city_uf", 'setup_campaign_id as city_setup_campaign_id')
vehicle_city = vehicles.join(worker_cityb, ((vehicles.cidade == worker_cityb.city_nome) & (vehicles.uf == worker_cityb.city_uf) & (worker_cityb.city_setup_campaign_id == vehicles.setup_campaign_id)))
worker_vehicle_city = vehicle_city.select('city_id', 'marca', 'modelo', 'anos', 'cidade', 'quantidade', 'valores', 'cores', 'setup_campaign_id', 'zerokm')

db_options["dbtable"] = "worker_vehicle_city"
worker_vehicle_city_current = glueContext.create_dynamic_frame_from_options('postgresql', connection_options = db_options)
worker_vehicle_city_current_df = worker_vehicle_city_current.toDF()
worker_vehicle_city_new = worker_vehicle_city.alias("new") \
  .join(worker_vehicle_city_current_df.alias("existing"), ((worker_vehicle_city_current_df.marca == worker_vehicle_city.marca) & (worker_vehicle_city_current_df.modelo == worker_vehicle_city.modelo) & (worker_vehicle_city_current_df.city_id == worker_vehicle_city.city_id) & (worker_vehicle_city_current_df.setup_campaign_id == worker_vehicle_city.setup_campaign_id) & (worker_vehicle_city_current_df.zerokm == worker_vehicle_city.zerokm)), "left_outer") \
  .where(worker_vehicle_city_current_df.id.isNull()) \
  .select("new.*")

worker_vehicle_city_dy = DynamicFrame.fromDF(worker_vehicle_city_new, glueContext, "worker_vehicle_city_dy")
worker_vehicle_city_sy = glueContext.write_dynamic_frame.from_jdbc_conf(frame = worker_vehicle_city_dy, catalog_connection = args['db_connection'], connection_options = {"dbtable": "worker_vehicle_city", "database": args['db_name']}, transformation_ctx = "worker_vehicle_city_sy")

db_options["dbtable"] = "worker_vehicle_city"
worker_vehicle_city = glueContext.create_dynamic_frame_from_options('postgresql', connection_options = db_options).toDF()
worker_vehicle_city = worker_vehicle_city.filter('setup_campaign_id IN (%s)'  % ('0' if not setup_campaigns_df_ids else ",".join(str(v) for v in setup_campaigns_df_ids)))

worker_cityc = worker_city.selectExpr("id as ccity_id", "uf")
worker_vehicle_city_uf = worker_vehicle_city.join(worker_cityc, worker_vehicle_city.city_id == worker_cityc.ccity_id)
worker_vehicle_city_uf = worker_vehicle_city_uf.selectExpr('id as vehicle_id','city_id','marca','modelo','anos','cidade','quantidade','valores','cores','setup_campaign_id','zerokm','uf')

workerufd = worker_vehicle_city_uf.selectExpr('vehicle_id','city_id','marca as ma','modelo as mo','anos as an','cidade as ci','quantidade as qu','valores as va','cores as co','setup_campaign_id as si','zerokm', 'uf as suf')
etl_formated = setups.join(workerufd, ((setups.cidade == workerufd.ci) & (setups.uf == workerufd.suf) & (workerufd.si == setups.setup_campaign_id)))
worker_etl_formated = etl_formated.select('id','setup_campaign_id','city_id','vehicle_id','name','message','thumbnail_url','value','page_url','km','ano_modelo','ano_fabricacao','blindado','uf','cidade','nome_cor','categoria','vendedor','make','model','version','inventory','zerokm')

db_options["dbtable"] = "worker_etl_formated"
worker_etl_formated_current = glueContext.create_dynamic_frame_from_options('postgresql', connection_options = db_options)
worker_etl_formated_current_df = worker_etl_formated_current.toDF()
worker_etl_formated_new = worker_etl_formated.alias("new") \
  .join(worker_etl_formated_current_df.alias("existing"), (worker_etl_formated_current_df.id == worker_etl_formated.id), "left_outer") \
  .where(worker_etl_formated_current_df.id.isNull()) \
  .select("new.*")

# persist worker_etl_formated
worker_etl_formated_dy = DynamicFrame.fromDF(worker_etl_formated_new.dropDuplicates(['id']), glueContext, "worker_etl_formated_dy")

worker_etl_formated_dye = ResolveChoice.apply(frame = worker_etl_formated_dy, choice = "make_cols", transformation_ctx = "worker_etl_formated_dye")

worker_etl_formated_dyes = DropNullFields.apply(frame = worker_etl_formated_dye, transformation_ctx = "worker_etl_formated_dyes")

worker_etl_formated_sy = glueContext.write_dynamic_frame.from_jdbc_conf(frame = worker_etl_formated_dyes, catalog_connection = args['db_connection'], connection_options = {"dbtable": "worker_etl_formated", "database": args['db_name']}, transformation_ctx = "worker_etl_formated_sy")

# set state transformed
db_options["dbtable"] = "sync_states_histories"
sync_states_histories = glueContext.create_dynamic_frame_from_options('postgresql', connection_options = db_options)
sync_states_histories_df = sync_states_histories.toDF()

sync_states_histories_transforming = sync_states_histories_df.filter("current = True AND state = 'transforming' AND setup_id IN (%s)"  % ('0' if not setup_campaigns_df_ids else ",".join(str(v) for v in setup_campaigns_df_ids)))
sync_states_histories_transforming_ids = [v.id for v in sync_states_histories_transforming.select('id').collect()]
sync_states_histories_transforming_setup_ids = sync_states_histories_transforming.select('setup_id').collect()

conn = pg8000.connect(host=db_host, port=int(db_port), database=args['db_name'], user=db_options["user"], password=db_options["password"])
cur = conn.cursor()
cur.execute("update setup_campaigns set sync_state='transformed' where sync_state='transforming' AND id IN (%s)" % ('0' if not setup_campaigns_df_ids else ",".join(str(v) for v in setup_campaigns_df_ids)))
cur.execute("update sync_states_histories set current='f', finished_at = NOW() where current='t' AND id IN (%s)" % ('0' if not sync_states_histories_transforming_ids else ",".join(str(v) for v in sync_states_histories_transforming_ids)))
cur.execute("update sync_states_histories set current='f', finished_at = NOW() where current='t' AND setup_id IN (%s)" % ('0' if not scope_setup_ids else ",".join(str(v) for v in scope_setup_ids)))
for setup_id in sync_states_histories_transforming_setup_ids:
    cur.execute("insert into sync_states_histories (state, setup_id, created_at, updated_at) values ('transformed', %d, NOW(), NOW())" % setup_id)
cur.execute("update day_histories set current='f', finished_at = NOW() where state='transforming'")
cur.execute("insert into day_histories (state, created_at, updated_at) values ('transformed', NOW(), NOW())")
conn.commit()
cur.close()
conn.close()

################################## INSTITUCIONAL feed geral

# load setups

setup_campaigns_entreprise_pre_df = setup_campaigns_all_df.filter(setup_campaigns_all_df.sync_state.isin(['transformed','transforming','loading','loaded','waiting', 'complete']))
setup_campaigns_entreprise_df = setup_campaigns_pre_df.filter('feirao = False')
setup_campaigns_df_entreprise_ids = [v.setup_campaign_id for v in setup_campaigns_entreprise_df.select('setup_campaign_id').collect()]

# set state to waiing

conn = pg8000.connect(host=db_host, port=int(db_port), database=args['db_name'], user=db_options["user"], password=db_options["password"])
cur = conn.cursor()
cur.execute("update setup_campaigns set sync_state='transforming' where id IN (%s)" % ('0' if not setup_campaigns_df_entreprise_ids else ",".join(str(v) for v in setup_campaigns_df_entreprise_ids)))
cur.execute("update sync_states_histories set current='f', finished_at = NOW() where current='t' AND setup_id IN (%s)" % ('0' if not scope_setup_ids else ",".join(str(v) for v in scope_setup_ids)))
for setup_id in setup_campaigns_df_entreprise_ids:
    cur.execute("insert into sync_states_histories (state, setup_id, created_at, updated_at) values ('transforming', %d, NOW(), NOW())" % setup_id)
cur.execute("update day_histories set current='f', finished_at = NOW() where current='t'")
cur.execute("insert into day_histories (state, created_at, updated_at) values ('transforming', NOW(), NOW())")
conn.commit()
cur.close()
conn.close()

# load target setup ids
join_setup_entreprise = target_csv_df.join(setup_campaigns_entreprise_df, f.trim(target_csv_df.uf) == f.trim(setup_campaigns_entreprise_df.setup_uf))
setups_entreprise = join_setup_entreprise.selectExpr('id','name','categoryid','message','thumbnail_url','value','page_url','CAST(inventory as INTEGER) as inventory','margin','brand','modelo','versao','km','ano_modelo','ano_fabricacao','blindado','uf','cidade','cor','nome_cor','categoria','vendedor','make','model','version','valor','catpreco','feirao','feiroes','cnpj','razao_social','info_neworold','binary_one','adjust_percentage_one','random_one','binary_two','adjust_percentage_two','random_two','latitude','longitude','setup_campaign_id')
vehicles_entreprise = setups_entreprise \
    .selectExpr("make as marca", "model as modelo", 'cidade', 'uf', 'setup_campaign_id', 'ano_fabricacao', 'nome_cor', 'value', 'inventory', "(CASE WHEN info_neworold = 'Novo' THEN true ELSE false END) as zerokm") \
    .groupBy('marca', 'modelo', 'cidade', 'uf', 'setup_campaign_id', 'zerokm') \
    .agg(f.concat_ws(";", f.collect_set(setups_entreprise.ano_fabricacao)).alias('anos'), \
    f.concat_ws(";", f.collect_set(setups_entreprise.nome_cor)).alias('cores'), \
    f.concat_ws(";", f.collect_set(setups_entreprise.value)).alias('valores'), \
    f.sum('inventory').alias('quantidade'))

cities_entreprise = vehicles_entreprise.groupBy('cidade', 'uf', 'setup_campaign_id').count().select('cidade', 'uf', 'setup_campaign_id')

# persist worker_city
worker_city_entreprise = cities_entreprise.selectExpr("cidade as nome", "uf", 'setup_campaign_id')
db_options["dbtable"] = "worker_city"
worker_city_current = glueContext.create_dynamic_frame_from_options('postgresql', connection_options = db_options)
worker_city_current_df = worker_city_current.toDF()

worker_city_entreprise_new = worker_city_entreprise.alias("new") \
  .join(worker_city_current_df.alias("existing"), ((worker_city_current_df.nome == worker_city_entreprise.nome) & (worker_city_current_df.uf == worker_city_entreprise.uf) & (worker_city_current_df.setup_campaign_id == worker_city_entreprise.setup_campaign_id)), "left_outer") \
  .where(worker_city_current_df.id.isNull()) \
  .select("new.*")

worker_city_entreprise_dy = DynamicFrame.fromDF(worker_city_entreprise_new, glueContext, "worker_city_entreprise_dy")
worker_city_entreprise_sy = glueContext.write_dynamic_frame.from_jdbc_conf(frame = worker_city_entreprise_dy, catalog_connection = args['db_connection'], connection_options = {"dbtable": "worker_city", "database": args['db_name']}, transformation_ctx = "worker_city_entreprise_sy")

worker_city = glueContext.create_dynamic_frame_from_options('postgresql', connection_options = db_options).toDF()
worker_city = worker_city.filter('setup_campaign_id IN (%s)'  % ('0' if not setup_campaigns_df_entreprise_ids else ",".join(str(v) for v in setup_campaigns_df_entreprise_ids)))

# persist vehicle_city
worker_cityb = worker_city.selectExpr("id as city_id", "nome as city_nome", "uf as city_uf", 'setup_campaign_id as city_setup_campaign_id')
vehicle_city = vehicles_entreprise.join(worker_cityb, ((vehicles_entreprise.cidade == worker_cityb.city_nome) & (vehicles_entreprise.uf == worker_cityb.city_uf) & (worker_cityb.city_setup_campaign_id == vehicles_entreprise.setup_campaign_id)))
worker_vehicle_city = vehicle_city.select('city_id', 'marca', 'modelo', 'anos', 'cidade', 'quantidade', 'valores', 'cores', 'setup_campaign_id', 'zerokm')

db_options["dbtable"] = "worker_vehicle_city"
worker_vehicle_city_current = glueContext.create_dynamic_frame_from_options('postgresql', connection_options = db_options)
worker_vehicle_city_current_df = worker_vehicle_city_current.toDF()
worker_vehicle_city_new = worker_vehicle_city.alias("new") \
  .join(worker_vehicle_city_current_df.alias("existing"), ((worker_vehicle_city_current_df.marca == worker_vehicle_city.marca) & (worker_vehicle_city_current_df.modelo == worker_vehicle_city.modelo) & (worker_vehicle_city_current_df.city_id == worker_vehicle_city.city_id) & (worker_vehicle_city_current_df.setup_campaign_id == worker_vehicle_city.setup_campaign_id) & (worker_vehicle_city_current_df.zerokm == worker_vehicle_city.zerokm)), "left_outer") \
  .where(worker_vehicle_city_current_df.id.isNull()) \
  .select("new.*")

worker_vehicle_city_entreprise_dy = DynamicFrame.fromDF(worker_vehicle_city_new, glueContext, "worker_vehicle_city_entreprise_dy")
worker_vehicle_city_entreprise_sy = glueContext.write_dynamic_frame.from_jdbc_conf(frame = worker_vehicle_city_entreprise_dy, catalog_connection = args['db_connection'], connection_options = {"dbtable": "worker_vehicle_city", "database": args['db_name']}, transformation_ctx = "worker_vehicle_city_entreprise_sy")

worker_vehicle_city = glueContext.create_dynamic_frame_from_options('postgresql', connection_options = db_options).toDF()
worker_vehicle_city = worker_vehicle_city.filter('setup_campaign_id IN (%s)'  % ('0' if not setup_campaigns_df_entreprise_ids else ",".join(str(v) for v in setup_campaigns_df_entreprise_ids)))

worker_cityc = worker_city.selectExpr("id as ccity_id", "uf")
worker_vehicle_city_uf = worker_vehicle_city.join(worker_cityc, worker_vehicle_city.city_id == worker_cityc.ccity_id)
worker_vehicle_city_uf = worker_vehicle_city_uf.selectExpr('id as vehicle_id','city_id','marca','modelo','anos','cidade','quantidade','valores','cores','setup_campaign_id','zerokm','uf')

workerufd = worker_vehicle_city_uf.selectExpr('vehicle_id','city_id','marca as ma','modelo as mo','anos as an','cidade as ci','quantidade as qu','valores as va','cores as co','setup_campaign_id as si','zerokm', 'uf as suf')
etl_formated = setups_entreprise.join(workerufd, ((setups_entreprise.cidade == workerufd.ci) & (setups_entreprise.uf == workerufd.suf) & (workerufd.si == setups_entreprise.setup_campaign_id)))
worker_etl_formated = etl_formated.select('id','setup_campaign_id','city_id','vehicle_id','name','message','thumbnail_url','value','page_url','km','ano_modelo','ano_fabricacao','blindado','uf','cidade','nome_cor','categoria','vendedor','make','model','version','inventory','zerokm')

db_options["dbtable"] = "worker_etl_formated"

worker_etl_formated_current = glueContext.create_dynamic_frame_from_options('postgresql', connection_options = db_options)
worker_etl_formated_current_df = worker_etl_formated_current.toDF()
worker_etl_formated_new = worker_etl_formated.alias("new") \
  .join(worker_etl_formated_current_df.alias("existing"), (worker_etl_formated_current_df.id == worker_etl_formated.id), "left_outer") \
  .where(worker_etl_formated_current_df.id.isNull()) \
  .select("new.*")

# persist worker_etl_formated
worker_etl_formated_entreprise_dy = DynamicFrame.fromDF(worker_etl_formated_new.dropDuplicates(['id']), glueContext, "worker_etl_formated_entreprise_dy")

worker_etl_formated_entreprise_dye = ResolveChoice.apply(frame = worker_etl_formated_entreprise_dy, choice = "make_cols", transformation_ctx = "worker_etl_formated_entreprise_dye")

worker_etl_formated_entreprise_dyes = DropNullFields.apply(frame = worker_etl_formated_entreprise_dye, transformation_ctx = "worker_etl_formated_entreprise_dyes")

worker_etl_formated_entreprise_sy = glueContext.write_dynamic_frame.from_jdbc_conf(frame = worker_etl_formated_entreprise_dyes, catalog_connection = args['db_connection'], connection_options = {"dbtable": "worker_etl_formated", "database": args['db_name']}, transformation_ctx = "worker_etl_formated_entreprise_sy")

# set state transformed
db_options["dbtable"] = "sync_states_histories"
sync_states_histories = glueContext.create_dynamic_frame_from_options('postgresql', connection_options = db_options)
sync_states_histories_df = sync_states_histories.toDF()

sync_states_histories_transforming = sync_states_histories_df.filter("current = True AND state = 'transforming' AND setup_id IN (%s)"  % ('0' if not setup_campaigns_df_entreprise_ids else ",".join(str(v) for v in setup_campaigns_df_entreprise_ids)))
sync_states_histories_transforming_ids = [v.id for v in sync_states_histories_transforming.select('id').collect()]
sync_states_histories_transforming_setup_ids = sync_states_histories_transforming.select('setup_id').collect()

conn = pg8000.connect(host=db_host, port=int(db_port), database=args['db_name'], user=db_options["user"], password=db_options["password"])
cur = conn.cursor()
cur.execute("update setup_campaigns set sync_state='transformed' where sync_state='transforming' AND id IN (%s)" % ('0' if not setup_campaigns_df_entreprise_ids else ",".join(str(v) for v in setup_campaigns_df_entreprise_ids)))
cur.execute("update sync_states_histories set current='f', finished_at = NOW() where current='t' AND id IN (%s)" % ('0' if not sync_states_histories_transforming_ids else ",".join(str(v) for v in sync_states_histories_transforming_ids)))
cur.execute("update sync_states_histories set current='f', finished_at = NOW() where current='t' AND setup_id IN (%s)" % ('0' if not scope_setup_ids else ",".join(str(v) for v in scope_setup_ids)))
for setup_id in sync_states_histories_transforming_setup_ids:
    cur.execute("insert into sync_states_histories (state, setup_id, created_at, updated_at) values ('transformed', %d, NOW(), NOW())" % setup_id)
cur.execute("update day_histories set current='f', finished_at = NOW() where state='transforming'")
cur.execute("insert into day_histories (state, created_at, updated_at) values ('transformed', NOW(), NOW())")
conn.commit()
cur.close()
conn.close()

### The end

job.commit()